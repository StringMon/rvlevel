package net.smartrig.level;

import android.graphics.PointF;

import net.smartrig.MainActivity;

/**
 * Core class that computes a leveling solution given vehicle tilt data, and exposes it in the
 * [left|right][Fornt|Rear] fields.
 *
 * Created by Sterling on 2015-01-19.
 */
public class Heights {
    // Array indices
    public static final int LEFT_FRONT  = 0;
    public static final int RIGHT_FRONT = 1;
    public static final int LEFT_REAR   = 2;
    public static final int RIGHT_REAR  = 3;

    // The arrangement of blocks needed to level to a given height
    public static final int[][] BLOCK_LAYOUTS = new int[][] {
        {1},
        {2, 1},
        {3, 2, 1},
        {3, 2, 1, 1},
        {3, 2, 2, 1, 1},
        {4, 3, 2, 2, 1, 1},
        {4, 3, 3, 2, 2, 1, 1}
    };

    // The result fields
    public float leftFront = 0,
            rightFront = 0,
            leftRear = 0,
            rightRear = 0;
    public Heights bestRounding = this;

    private boolean rounded = false;
    private float lateral, longitudinal, softness;
    
    private Heights() {}
    
    public Heights(float newLateral, float newLongitudinal, float newSoftness) {
        // This is the main constructor: call it with tilt & softness values, and it will compute
        // a leveling solution before returning.
        
        lateral        = newLateral;
        longitudinal = newLongitudinal;
        softness     = newSoftness;
        
        float stiffnessRatio = MainActivity.rearGAWR / MainActivity.frontGAWR;

        // Adjust for wheelbase - this is a guess based on experimentation with our levels
        longitudinal *= MainActivity.wheelbase / 11f;
        
        // Bring low sides up
        if (lateral > 0) {
            leftFront += lateral;
            leftRear  += lateral;
        } else {
            rightFront += -lateral;
            rightRear  += -lateral;
        }
        if (longitudinal > 0) {
            leftFront  += longitudinal;
            rightFront += longitudinal;
        } else {
            leftRear  += -longitudinal;
            rightRear += -longitudinal;
        }
        
        // Reduce opposite corners
        if ((leftRear > 0) && (rightFront > 0)) {
            if (rightFront / stiffnessRatio > leftRear) {
                rightFront -= leftRear * stiffnessRatio;
                leftRear    = 0;
            } else {
                leftRear  -= rightFront / stiffnessRatio;
                rightFront = 0;
            }
        } else if ((leftFront > 0) && (rightRear > 0)) {
            if (leftFront / stiffnessRatio > rightRear) {
                leftFront -= rightRear * stiffnessRatio;
                rightRear  = 0;
            } else {
                rightRear -= leftFront / stiffnessRatio;
                leftFront  = 0;
            }
        }
        
        PointF errorAfterReduce = computeError();

        // Compute correction for longitudinal error introduced during reduction
        float rawFront = 0, rawRear = 0;
        if (longitudinal < 0) {
            rawRear  = errorAfterReduce.y;
        } else if (longitudinal > 0) {
            rawFront = errorAfterReduce.y;
        }
        float limitedRear  = Math.min(rawRear, Math.min(leftRear, rightRear));
        float combinedFront = rawFront + rawRear - limitedRear;
        
        // Apply correction
        leftFront  += combinedFront;
        rightFront += combinedFront;
        leftRear   -= limitedRear;
        rightRear  -= limitedRear;
        
        // Adjust for surface softness
        if (leftFront > 0) {
            leftFront += softness;
        }
        if (rightFront > 0) {
            rightFront += softness;
        }
        if (leftRear > 0) {
            leftRear += softness;
        }
        if (rightRear > 0) {
            rightRear += softness;
        }

        // Rounding
        float thisFitness; 
        float bestFitness = Float.MAX_VALUE;
        Heights rounded;
        for (int i = 0; i < 16; i++) {
            rounded = this.round(i);
            thisFitness = rounded.computeFitness();
            if (thisFitness < bestFitness) {
                bestFitness = thisFitness;
                bestRounding = rounded;
            }
        }
    }

    private PointF computeError() {
        // x is lateral error, y is longitudinal error
        
        float totalGAWR = MainActivity.frontGAWR + MainActivity.rearGAWR;
        float frontStiffness = MainActivity.frontGAWR / totalGAWR;
        float rearStiffness  = MainActivity.rearGAWR / totalGAWR;

        PointF result = new PointF();
        result.x = (lateral - (realHeight(leftFront) - realHeight(rightFront))) * frontStiffness +
                    (lateral - (realHeight(leftRear) - realHeight(rightRear))) * rearStiffness;
        result.y = (longitudinal - (realHeight(leftFront) - realHeight(leftRear))) / 2f +
                    (longitudinal - (realHeight(rightFront) - realHeight(rightRear))) / 2f;

        return result;
    }
    
    private float realHeight(float value) {
        if (value > 0) {
            if (rounded) {
                return value + MainActivity.blockTop - softness;
            } else {
                return value;
            }
        } else {
            return value;
        }
    }
    
    public static int countBlocks(int height) {
        int result = 0;
        if (height >= BLOCK_LAYOUTS.length) {
            return 99;
        } else if (MainActivity.blockType.equals(MainActivity.BLOCK_TYPE_WOOD)) {
            return Math.round(height / MainActivity.blockSize);
        } else {
            if (height > 0) {
                height--; // array is 0-based
                for (int i = 0; i < BLOCK_LAYOUTS[height].length; i++) {
                    result += BLOCK_LAYOUTS[height][i];
                }
            }
            return result;
        }
    }
    
    private float computeFitness() {
        // Lower is better.
        PointF error = computeError();
        
        int[] counts = new int[] {
                countBlocks((int) leftFront),
                countBlocks((int) rightFront), 
                countBlocks((int) leftRear),
                countBlocks((int) rightRear)
        };
        
//        for (int c = 0; c < counts.length; c++) {
//            counts[c] += Math.pow(counts[c], 2) + Math.signum(counts[c]);
//        }
        
        double result = (Math.pow(error.x, 2) + Math.pow(error.y, 2)) * 30 +
                counts[0] + counts[1] +
                (counts[2] + counts[3]) * MainActivity.rearType;

        return (float) result;
    }
    
    @Override
    protected Heights clone() throws CloneNotSupportedException {
        super.clone();
        
        Heights result = new Heights();
        result.lateral        = this.lateral;
        result.longitudinal    = this.longitudinal;
        result.softness        = this.softness;
        result.leftFront    = this.leftFront;
        result.rightFront    = this.rightFront;
        result.leftRear        = this.leftRear;
        result.rightRear    = this.rightRear;
        return result;
    }

    private Heights round(int directions) {
        Heights result = new Heights();
        result.lateral        = this.lateral;
        result.longitudinal    = this.longitudinal;
        result.softness        = this.softness;
        result.leftFront    = round(this.leftFront, directions);
        result.rightFront    = round(this.rightFront, directions / 2);
        result.leftRear        = round(this.leftRear, directions / 4);
        result.rightRear    = round(this.rightRear, directions / 8);
        result.rounded = true;
        return result;
    }
    private float round(float value, int direction) {
        if (value <= MainActivity.blockTop) {
            return 0;
        }
        
        if (direction % 2 == 1) {
            // round down
            return MainActivity.blockSize * (float) Math.floor(value / MainActivity.blockSize - MainActivity.blockTop);
        } else {
            // round up
            return MainActivity.blockSize * (float) Math.ceil(value / MainActivity.blockSize - MainActivity.blockTop);
        }
    }
}
