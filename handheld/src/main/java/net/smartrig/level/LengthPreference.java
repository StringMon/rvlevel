/*
 * Copyright 2012 Jay Weisskopf
 *
 * Licensed under the MIT License (see LICENSE.txt)
 */

package net.smartrig.level;

import android.content.Context;
import android.content.res.TypedArray;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import net.smartrig.R;

/**
 * Preference subclass to support easy user selection of an approximate length value (e.g., ft+in)
 *
 * Created by Sterling on 2014-06-14.
 * Based on code from https://github.com/jayschwa/AndroidSliderPreference
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class LengthPreference extends DialogPreference {

    private final static int SEEKBAR_RESOLUTION = 10000;

    private float mValue;
    private int mSeekBarValue;

    public LengthPreference(Context context) {
        this(context, null);
    }
    public LengthPreference(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }
    public LengthPreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        setDialogLayoutResource(R.layout.slider_preference_dialog);
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return a.getFloat(index, 0);
    }

    @Override
    protected void onSetInitialValue(boolean restoreValue, Object defaultValue) {
        setValue(restoreValue ? getPersistedFloat(mValue) : (Float) defaultValue);
    }

    @Override
    public CharSequence getSummary() {
        return getSummary(mValue);
    }
    public CharSequence getSummary(float value) {
        StringBuilder result = new StringBuilder();
        
        float span = 26 - 8;
        value = 8 + value * span;    // length in feet
        int feet = (int) value;
        int inches = (int) ((value - feet) * 12);
        result.append(feet).append("' ").append(inches).append('"');
        
        long cm = Math.round((feet * 12 + inches) * 2.54);
        result.append("    ").append(cm).append("cm");
        
        return result;
    }

    public float getValue() {
        return mValue;
    }

    public void setValue(float value) {
        value = Math.max(0, Math.min(value, 1)); // clamp to [0, 1]
        if (shouldPersist()) {
            persistFloat(value);
        }
        if (value != mValue) {
            mValue = value;
            notifyChanged();
        }
    }

    @Override
    protected View onCreateDialogView() {
        mSeekBarValue = (int) (mValue * SEEKBAR_RESOLUTION);
        View view = super.onCreateDialogView();
        
        final TextView message = view.findViewById(android.R.id.message);
        message.setText(getSummary());
        message.setVisibility(View.VISIBLE);
        
        SeekBar seekbar = view.findViewById(R.id.slider_preference_seekbar);
        seekbar.setMax(SEEKBAR_RESOLUTION);
        seekbar.setProgress(mSeekBarValue);
        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    LengthPreference.this.mSeekBarValue = progress;
                    message.setText(getSummary((float) progress / SEEKBAR_RESOLUTION));
                }
            }
        });
        return view;
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        final float newValue = (float) mSeekBarValue / SEEKBAR_RESOLUTION;
        if (positiveResult && callChangeListener(newValue)) {
            setValue(newValue);
        }
        super.onDialogClosed(positiveResult);
    }
}
