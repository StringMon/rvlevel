package net.smartrig.ble;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import net.smartrig.R;

import java.util.UUID;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static net.smartrig.SmartRigCommon.ACCEL_SERVICE_UUID;

/**
 * A class to instantiate from the UI to scan for compatible BLE devices.
 * 
 * Created by Sterling on 2017-10-26.
 * Based on code from https://github.com/googlesamples/android-BluetoothLeGatt
 */
@SuppressWarnings("WeakerAccess")
public class BleScanner {
//    private static final BaseApp.LogFlag DOLOG = BaseApp.DOLOG;
//    private static final String TAG = BleScanner.class.getSimpleName();

    public static final int REQUEST_ENABLE_BT = 1;
    public static final int REQUEST_LOCATION_PERMISSION = 2;
    private Runnable stopper;

    public interface ScanCallback extends BluetoothAdapter.LeScanCallback {
        void onLeScanTimeout();
    }
    // Stops scanning after 10 seconds.
    private static final long SCAN_PERIOD = 10000;

    public boolean isScanning = false;

    private BluetoothAdapter mBluetoothAdapter;

    private final Handler stopHandler = new Handler();
    private AppCompatActivity parent;
    public BleScanner(AppCompatActivity activity) {
        parent = activity;

        // Determine whether BLE is supported on the device
        if (!activity.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(activity, R.string.ble_not_supported, Toast.LENGTH_SHORT).show();
            return;
        }

        // Initializes a Bluetooth adapter.  For API level 18 and above, get a reference to
        // BluetoothAdapter through BluetoothManager.
        final BluetoothManager bluetoothManager =
                (BluetoothManager) activity.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        // Checks if Bluetooth is supported on the device.
        if (mBluetoothAdapter == null) {
            Toast.makeText(activity, R.string.error_bluetooth_not_supported, Toast.LENGTH_SHORT).show();
        }
    }

    public boolean startScan(final ScanCallback callback) {
        if (mBluetoothAdapter == null) {
            // either Bluetooth or BLE is not supported
            return false;
        }
        
        // Ensures Bluetooth is enabled on the device.  If Bluetooth is not currently enabled,
        // fire an intent to display a dialog asking the user to grant permission to enable it.
        if (!mBluetoothAdapter.isEnabled()) {
            isScanning = false;
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            parent.startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            return false;
        }

        if (ContextCompat.checkSelfPermission(parent, ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            // We don't hold LOCATION permission, which is necessary to scan for BT devices.

            if (ActivityCompat.shouldShowRequestPermissionRationale(parent,
                            ACCESS_COARSE_LOCATION)) {
                // Show an expanation to the user 
                new AlertDialog.Builder(parent)
                        .setMessage(R.string.rationale_location)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                ActivityCompat.requestPermissions(parent, 
                                        new String[] {ACCESS_COARSE_LOCATION}, 
                                        REQUEST_LOCATION_PERMISSION);
                                dialog.cancel();
                            }
                        })
                        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                parent.onRequestPermissionsResult(REQUEST_LOCATION_PERMISSION,
                                        new String[0], new int[0]);
                                dialog.cancel();
                            }
                        })
                        .show();
            } else {
                ActivityCompat.requestPermissions(parent, new String[] {ACCESS_COARSE_LOCATION}, 
                        REQUEST_LOCATION_PERMISSION);
            }
        }
        
        // All checks look good. Proceed to scan.
        
        isScanning = true;

        // Stops scanning after a pre-defined scan period.
        synchronized (stopHandler) {
            stopper = new Runnable() {
                @Override
                public void run() {
                    stopScan(callback);
                    callback.onLeScanTimeout();
                }
            };
            stopHandler.postDelayed(stopper, SCAN_PERIOD);
        }
        
        mBluetoothAdapter.startLeScan(new UUID[]{ACCEL_SERVICE_UUID}, callback);
    
        return isScanning;
    }

    public void stopScan(ScanCallback callback) {
        isScanning = false;
        synchronized (stopHandler) {
            if (stopper != null) {
                stopHandler.removeCallbacks(stopper);
                stopper = null;
            }
        }
        if (mBluetoothAdapter != null) {
            mBluetoothAdapter.stopLeScan(callback);
        }
    }
}
