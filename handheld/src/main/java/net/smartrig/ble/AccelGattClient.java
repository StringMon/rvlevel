package net.smartrig.ble;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.util.Log;

import static android.content.Context.BLUETOOTH_SERVICE;
import static net.smartrig.SmartRigCommon.ACCEL_SERVICE_UUID;
import static net.smartrig.SmartRigCommon.CHARACTERISTIC_ACCELS_UUID;
import static net.smartrig.SmartRigCommon.DESCRIPTOR_CONFIG_UUID;

/**
 * Client to connect to my Android Things BLE accelerometer server
 *
 * Created by Sterling on 2017-10-25.
 * Based on code from https://github.com/Nilhcem/blefun-androidthings
 */
public class AccelGattClient {

    private static final String TAG = AccelGattClient.class.getSimpleName();

    public interface OnAccelListener {
        void onAccelRead(int[] values);
        void onConnected(boolean success);
    }

    public boolean isConnected = false;
    
    private final Context context;
    private final OnAccelListener listener;
    private final String deviceAddress;

    private final BluetoothManager bluetoothManager;
    private final BluetoothAdapter bluetoothAdapter;
    private BluetoothGatt myGatt;
    private BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                Log.i(TAG, "Connected to GATT client. Attempting to start service discovery");
                gatt.discoverServices();
                myGatt = gatt;
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                Log.i(TAG, "Disconnected from GATT client");
                isConnected = false;
                listener.onConnected(false);
                myGatt = null;
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            isConnected = false;
            if (status == BluetoothGatt.GATT_SUCCESS) {
                // When accelerometer service is found, subscribe to its data stream
                myGatt = gatt;
                isConnected = subscribeToAccels(true);
                listener.onConnected(isConnected);
            } else {
                Log.w(TAG, "onServicesDiscovered received: " + status);
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            readAccelCharacteristic(characteristic);
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            readAccelCharacteristic(characteristic);
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            if (DESCRIPTOR_CONFIG_UUID.equals(descriptor.getUuid())) {
                BluetoothGattCharacteristic characteristic = gatt.getService(ACCEL_SERVICE_UUID)
                        .getCharacteristic(CHARACTERISTIC_ACCELS_UUID);
                gatt.readCharacteristic(characteristic);
            }
        }

        private void readAccelCharacteristic(BluetoothGattCharacteristic characteristic) {
            if (CHARACTERISTIC_ACCELS_UUID.equals(characteristic.getUuid())) {
                // The BLE interface sends data as ana array of bytes, but our accel data is natively
                // 3 ints (1 per axis). Reassemble the ints from the bytes.
                byte[] input = characteristic.getValue();
                int[] output = new int[3];
                for (int i = 0; i < input.length; i += 2) {
                    int lsb = input[i] & 0xff;
                    int msb = input[i + 1];
                    output[i / 2] = (msb << 8 | lsb);
                }
                listener.onAccelRead(output);
            }
        }
    };

    private final BroadcastReceiver mBluetoothReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.STATE_OFF);

            switch (state) {
                case BluetoothAdapter.STATE_ON:
                    startClient();
                    break;
                case BluetoothAdapter.STATE_OFF:
                    stopClient();
                    break;
                default:
                    // Do nothing
                    break;
            }
        }
    };

    public AccelGattClient(Context context, String deviceAddress, OnAccelListener listener) throws RuntimeException {
        this.context = context;
        this.listener = listener;
        this.deviceAddress = deviceAddress;

        bluetoothManager = (BluetoothManager) context.getSystemService(BLUETOOTH_SERVICE);
        bluetoothAdapter = bluetoothManager.getAdapter();
        if (!checkBluetoothSupport(bluetoothAdapter)) {
            throw new RuntimeException("GATT client requires Bluetooth support");
        }

        // Register for system Bluetooth events
        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        context.registerReceiver(mBluetoothReceiver, filter);
        if (!bluetoothAdapter.isEnabled()) {
            Log.w(TAG, "Bluetooth is currently disabled... enabling");
            bluetoothAdapter.enable();
        } else {
            Log.i(TAG, "Bluetooth enabled... starting client");
            startClient();
        }
    }

    public void close() {
        BluetoothAdapter bluetoothAdapter = bluetoothManager.getAdapter();
        if (bluetoothAdapter.isEnabled()) {
            stopClient();
        }

        try {
            context.unregisterReceiver(mBluetoothReceiver);
        } catch (Exception e) {
            // Probably just means that the receiver was never registered
        }
    }

    private boolean checkBluetoothSupport(BluetoothAdapter bluetoothAdapter) {
        if (bluetoothAdapter == null) {
            Log.w(TAG, "Bluetooth is not supported");
            return false;
        }

        if (!context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Log.w(TAG, "Bluetooth LE is not supported");
            return false;
        }

        return true;
    }

    private void startClient() {
        BluetoothDevice bluetoothDevice = bluetoothAdapter.getRemoteDevice(deviceAddress);
        myGatt = bluetoothDevice.connectGatt(context, false, mGattCallback);

        if (myGatt == null) {
            Log.w(TAG, "Unable to create GATT client");
        }
        isConnected = false;
    }

    private void stopClient() {
        if (myGatt != null) {
            myGatt.close();
            myGatt = null;
        }
        isConnected = false;
    }

    public void onPause() {
        if (isConnected) {
            subscribeToAccels(false);
        }
    }

    public void onResume() {
        if (isConnected) {
            subscribeToAccels(true);
        }
    }

    public void requestAccels() {
        if (isConnected) {
            BluetoothGattCharacteristic accelsRequest = myGatt
                    .getService(ACCEL_SERVICE_UUID)
                    .getCharacteristic(CHARACTERISTIC_ACCELS_UUID);
            myGatt.readCharacteristic(accelsRequest);
        }
    }

    private boolean subscribeToAccels(boolean enabled) {
        if (myGatt != null) {
            BluetoothGattService service = myGatt.getService(ACCEL_SERVICE_UUID);
            if (service != null) {
                BluetoothGattCharacteristic characteristic =
                        service.getCharacteristic(CHARACTERISTIC_ACCELS_UUID);
                if (characteristic != null) {
                    myGatt.setCharacteristicNotification(characteristic, enabled);

                    BluetoothGattDescriptor descriptor =
                            characteristic.getDescriptor(DESCRIPTOR_CONFIG_UUID);
                    if (descriptor != null) {
                        if (enabled) {
                            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                        } else {
                            descriptor.setValue(BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
                        }
                        return myGatt.writeDescriptor(descriptor);
                    }
                }
            }
        }
        
        return false;
    }
}
