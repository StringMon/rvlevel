package net.smartrig;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceFragment;

/**
 * Main Preferences for leveling.
 *
 * Created by Sterling on 2015-01-19.
 */
public class SettingsActivity extends Activity {
//    public static final String PREF_UNITS = "units";
    public static final String PREF_FRONT_GAWR = "front_gawr";  // GAWR == gross axle weight rating
    public static final String PREF_REAR_GAWR = "rear_gawr";
    public static final String PREF_REAR_TYPE = "rear_type";
    public static final String PREF_WHEELBASE = "wheelbase";
    public static final String PREF_BLOCK_TYPE = "block_type";
    public static final String PREF_BLOCK_SIZE = "block_size";
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Display the fragment as the main content.
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
    }
    
    public static class SettingsFragment extends PreferenceFragment 
            implements OnPreferenceChangeListener {
        private SharedPreferences settings;
        
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            // Load the preferences from an XML resource
            addPreferencesFromResource(R.xml.settings);
        }
        
        @Override
        public void onStart() {
            super.onStart();
            
            Activity activity = getActivity();
            settings = activity.getSharedPreferences(
                    activity.getPackageName() + "_preferences", Context.MODE_PRIVATE);

            findPreference(PREF_FRONT_GAWR).setOnPreferenceChangeListener(this);
            findPreference(PREF_REAR_GAWR).setOnPreferenceChangeListener(this);
            showGAWRs(activity);

            findPreference(PREF_BLOCK_TYPE).setOnPreferenceChangeListener(this);
            onPreferenceChange(findPreference(PREF_BLOCK_TYPE), settings.getString(
                    PREF_BLOCK_TYPE, getString(R.string.pref_block_type_default)));
        }
        
        private void showGAWRs(Activity activity) {
            String gawrSummary = activity.getString(R.string.pref_gawr_summary);
            findPreference(PREF_FRONT_GAWR).setSummary(gawrSummary + ' ' + 
                    settings.getString(PREF_FRONT_GAWR, getString(R.string.pref_front_gawr_default)));
            findPreference(PREF_REAR_GAWR).setSummary(gawrSummary + ' ' + 
                    settings.getString(PREF_REAR_GAWR, getString(R.string.pref_front_gawr_default)));
        }

        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            String key = preference.getKey();
            
            if (key.equals(PREF_FRONT_GAWR) ||
                key.equals(PREF_REAR_GAWR)) {
                preference.setSummary(getActivity().getString(R.string.pref_gawr_summary) + 
                        ' ' + newValue.toString());
                
            } else if (key.equals(PREF_BLOCK_TYPE)) {
                findPreference(PREF_BLOCK_SIZE).setEnabled(newValue.equals(MainActivity.BLOCK_TYPE_WOOD));
            }

            return true;
        }
    }
}
