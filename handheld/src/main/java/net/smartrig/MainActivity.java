package net.smartrig;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import net.smartrig.ble.AccelGattClient;
import net.smartrig.ble.BleScanner;
import net.smartrig.level.Heights;

import java.io.IOException;

import name.udell.common.BaseApp;
import name.udell.common.HelpActivity;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static net.smartrig.R.style.DayTheme;
import static net.smartrig.SmartRigCommon.ADXL345_1G;
import static net.smartrig.ble.BleScanner.REQUEST_LOCATION_PERMISSION;

/**
 * The primary leveling UI. Also controls the BLE link to the leveling sensor.
 * 
 * Created by Sterling on 2015-01-19 (as LevelActivity).
 */
public class MainActivity 
        extends AppCompatActivity 
        implements OnSeekBarChangeListener, 
                OnClickListener, 
                SurfaceHolder.Callback,
                AccelGattClient.OnAccelListener, 
                BleScanner.ScanCallback {
    private static final BaseApp.LogFlag DOLOG = BaseApp.DOLOG;
    private static final String TAG = MainActivity.class.getSimpleName();

    public static final String KEY_SHOW_BLOCKS = "show_blocks";
//    public static final String EXTRA_ADDRESS = "extra_address";

    public static final String BLOCK_TYPE_PLASTIC    = "plastic";
    public static final String BLOCK_TYPE_WOOD        = "wood";

    public static float frontGAWR = 6000, rearGAWR = 13500;
    public static int rearType = 2;    // wheels at each rear corner
    public static float wheelbase = 0.33f;
    public static String blockType = BLOCK_TYPE_PLASTIC;
    public static float blockSize = 1;
    public static float blockTop = 0.25f;

    private TextView lateralLabel, longitudinalLabel, surfaceLabel;
//    private TextView leftFrontHeight, rightFrontHeight, leftRearHeight, rightRearHeight;
    private TextView leftFrontBlocks, rightFrontBlocks, leftRearBlocks0, leftRearBlocks1, 
                        rightRearBlocks0, rightRearBlocks1, totalBlocks;
    private SeekBar lateralSeekBar, longitudinalSeekBar, surfaceSeekBar;
    private String unit = "\"";
    private Resources resources;
    private boolean showBlocks;
    private SharedPreferences settings;
    private Camera cam;
    private boolean isFlashOn = false;
    private LinearLayout layoutRoot;
    private SurfaceHolder mHolder;
    private int pluralResource = R.plurals.block;
    private AccelGattClient gattClient;
    private static String sensorAddress;
    private BleScanner bleScanner;
    private boolean calibrating = false;
    private float measured1g;
    private int currentThemeId;
    private boolean isStarted = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        resources = getResources();
        settings = getApplicationContext().getSharedPreferences(
                getPackageName() + "_preferences", Context.MODE_PRIVATE);

        // Select and initialize the theme
        final String themeName = settings.getString("theme", getString(R.string.theme_default));
        currentThemeId = themeName.equals(getString(R.string.theme_day)) 
                ? DayTheme : R.style.NightTheme;
        setTheme(currentThemeId);

        // Load the UI layout
        initUI();       //  MUST BE DONE AFTER setTheme()
        loadSettings(); //  and this MUST BE DONE AFTER initUI()
        
        // Init the progress area (this is mostly for the current-value labels)
        onProgressChanged(longitudinalSeekBar, longitudinalSeekBar.getMax() / 2, false);
        onProgressChanged(lateralSeekBar, lateralSeekBar.getMax() / 2, true);

        // Initialize link to accelerometer sensor
        bleScanner = new BleScanner(this);
        measured1g = settings.getFloat("calibrate_z", ADXL345_1G);
    }

    @Override
    protected void onStart() {
        super.onStart();
        
        isStarted = true;
        
        if (gattClient == null) {
            // Not currently connected to an accel sensor
            if (TextUtils.isEmpty(sensorAddress)) {
                bleScanner.startScan(this);
                showTopToast(R.string.scanning);
            } else {
                connectToSensor(sensorAddress);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (gattClient != null) {
            gattClient.onResume();
            gattClient.requestAccels();
        }
    }

    @Override
    protected void onPause() {
        if (gattClient != null) {
            gattClient.onPause();
        }

        super.onPause();
    }

    @Override
    protected void onStop() {
        isStarted = false;
        
        if (isFlashOn || (cam != null)) {
            // Flashlight is on
            toggleTorch();
        }

        
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        connectToSensor(null);
        
        super.onDestroy();
    }

    private void loadSettings() {
        wheelbase = settings.getFloat(SettingsActivity.PREF_WHEELBASE, 0.33f);
        wheelbase = 8 + wheelbase * (26 - 8);
        
        frontGAWR = Float.parseFloat(settings.getString(
                SettingsActivity.PREF_FRONT_GAWR, 
                getString(R.string.pref_front_gawr_default)));
        rearGAWR = Float.parseFloat(settings.getString(
                SettingsActivity.PREF_REAR_GAWR, 
                getString(R.string.pref_rear_gawr_default)));
        
        rearType = Integer.parseInt(settings.getString(
                SettingsActivity.PREF_REAR_TYPE, 
                getString(R.string.pref_rear_type_default)));
        switch (rearType) {
            case 1:
                leftRearBlocks1.setVisibility(View.INVISIBLE);
                rightRearBlocks1.setVisibility(View.INVISIBLE);
                break;
            case 2:
                leftRearBlocks1.setVisibility(View.VISIBLE);
                rightRearBlocks1.setVisibility(View.VISIBLE);
                break;
        }
        
        blockType = settings.getString(
                SettingsActivity.PREF_BLOCK_TYPE, 
                getString(R.string.pref_block_type_default));
        if (blockType.equals(BLOCK_TYPE_WOOD)) {
            blockSize = Float.parseFloat(settings.getString(
                    SettingsActivity.PREF_BLOCK_SIZE, 
                    getString(R.string.pref_block_size_default)));
            blockTop = 0;
            pluralResource = R.plurals.board;
        } else {
            blockSize = 1;
            blockTop  = 0.25f;
            pluralResource = R.plurals.block;
        }
        totalBlocks.setText(getString(R.string.total_blocks, 
                resources.getQuantityString(pluralResource, 0),
                0f));
    }
    
    private void initUI() {
        setContentView(R.layout.level_activity);

        if (currentThemeId == DayTheme) {
            findViewById(R.id.output_area).setBackgroundResource(R.drawable.rig_day);
        } else {
            findViewById(R.id.output_area).setBackgroundResource(R.drawable.rig_night);
        }

        lateralLabel = findViewById(R.id.lateral_label);
        longitudinalLabel = findViewById(R.id.longitudinal_label);
        surfaceLabel = findViewById(R.id.surface_label);

//        leftFrontHeight        = (TextView) findViewById(R.id.left_front_height);
//        rightFrontHeight    = (TextView) findViewById(R.id.right_front_height);
//        leftRearHeight        = (TextView) findViewById(R.id.left_rear_height);
//        rightRearHeight        = (TextView) findViewById(R.id.right_rear_height);

        leftFrontBlocks = findViewById(R.id.left_front_blocks);
        rightFrontBlocks = findViewById(R.id.right_front_blocks);
        leftRearBlocks0 = findViewById(R.id.left_rear_blocks_0);
        rightRearBlocks0 = findViewById(R.id.right_rear_blocks_0);
        leftRearBlocks1 = findViewById(R.id.left_rear_blocks_1);
        rightRearBlocks1 = findViewById(R.id.right_rear_blocks_1);
        totalBlocks = findViewById(R.id.total_blocks_label);
        totalBlocks.setText(getString(R.string.total_blocks,
                resources.getQuantityString(pluralResource, 0),
                0f));

        lateralSeekBar = findViewById(R.id.lateral_bubble);
        lateralSeekBar.setOnSeekBarChangeListener(this);
        longitudinalSeekBar = findViewById(R.id.longitudinal_bubble);
        longitudinalSeekBar.setOnSeekBarChangeListener(this);
        surfaceSeekBar = findViewById(R.id.surface);
        surfaceSeekBar.setOnSeekBarChangeListener(this);

        showBlocks = true;//settings.getBoolean(KEY_SHOW_BLOCKS, true);

        setIncrementListeners(R.id.longitudinal_labels);
        setIncrementListeners(R.id.lateral_labels);

        layoutRoot = findViewById(R.id.layout_root);
        layoutRoot.getViewTreeObserver().addOnGlobalLayoutListener(layoutListener);

        if ((gattClient != null) && gattClient.isConnected) {
            lateralSeekBar.setEnabled(false);
            longitudinalSeekBar.setEnabled(false);
        }
    }

    private OnGlobalLayoutListener layoutListener = new OnGlobalLayoutListener() {
        @SuppressWarnings("deprecation")
        @Override
        public void onGlobalLayout() {
            layoutRoot.getViewTreeObserver().removeGlobalOnLayoutListener(this);

            // When layout is completed, set up the output area based on window size.
            
            final DisplayMetrics metrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(metrics);

            View outputArea = findViewById(R.id.output_area);
            if (outputArea.getWidth() > 400 * metrics.density) {
                outputArea.getLayoutParams().width = (int) (400 * metrics.density);
            }
        }
    };

    private void setIncrementListeners(int id) {
        // The user can increment decrement the level SeekBars by tapping on the labels at their
        // ends. This sets up the onClick listeners to make that happen.
        RelativeLayout labels = findViewById(id);
        for (int i = 0; i < labels.getChildCount(); i++) {
            View child = labels.getChildAt(i);
            if (child instanceof TextView) {
                child.setOnClickListener(this);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.level, menu);
        
//        setUnitsIcon(menu.findItem(R.id.menu_units));

        // detect camera not supported
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)) {
            menu.removeItem(R.id.menu_torch);
        }
        
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem torchItem = menu.findItem(R.id.menu_torch);
        if (torchItem != null) {
            if (isFlashOn || (cam != null)) {
                // Flashlight is on
                torchItem.setIcon(R.drawable.ic_torch_off_48dp);
            } else {
                torchItem.setIcon(R.drawable.ic_torch_on_48dp);
            }
        }

        // Adapt the theme toggle icon based on the current theme
        MenuItem themeItem = menu.findItem(R.id.menu_theme);
        if (currentThemeId == DayTheme) {
            themeItem.setIcon(R.drawable.ic_brightness_3_white_48dp);
        } else {
            themeItem.setIcon(R.drawable.ic_brightness_low_white_48dp);
        }

        // Adapt the sensor icon based on connection state
        MenuItem connectItem = menu.findItem(R.id.menu_connect);
        if (bleScanner.isScanning) {
            connectItem.setActionView(R.layout.actionbar_indeterminate_progress);
            connectItem.setIcon(null);
        } else {
            connectItem.setActionView(null);
            if ((gattClient != null) && gattClient.isConnected) {
                connectItem.setIcon(R.drawable.ic_level_connected_48dp);
            } else {
                connectItem.setIcon(R.drawable.ic_level_disconnected_48dp);
            }
        }

        // Calibration only make sense if we're connected to a sensor
        menu.findItem(R.id.menu_calibrate).setEnabled((gattClient != null) && gattClient.isConnected);

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
//        case R.id.menu_units:
//            showBlocks = !showBlocks;
//            settings.edit().putBoolean(KEY_SHOW_BLOCKS, showBlocks).apply();
//            setUnitsIcon(item);
//            onProgressChanged(null, 0, false);
//            return true;

            case R.id.menu_connect:
//                startActivityForResult(new Intent(this, DeviceScanActivity.class), R.id.menu_connect & 0xff);
                if (!bleScanner.isScanning) {
                    if ((gattClient == null) || TextUtils.isEmpty(sensorAddress)) {
                        bleScanner.startScan(this);
                        showTopToast(R.string.scanning);
                    } else if (!gattClient.isConnected) {
                        connectToSensor(sensorAddress);
                        showTopToast(R.string.connecting);
                    } else {
                        connectToSensor(null);
                        showTopToast(R.string.disconnected);
                    }
                    invalidateOptionsMenu();
                }
                return true;
            
            case R.id.menu_torch:
                toggleTorch();
                return true;
                
            case R.id.menu_theme:
                toggleTheme();
                return true;
            
            case R.id.menu_calibrate:
                calibrateSensor();
                return true;

            case R.id.menu_open_source:
                startActivity(new Intent(this, HelpActivity.class)
                        .putExtra("url", "file:///android_asset/open_source.html"));
                return true;

            case R.id.menu_settings:
                startActivityForResult(new Intent(this, SettingsActivity.class), R.id.menu_settings & 0xff);
                return true;
        }
        
        return super.onOptionsItemSelected(item);
    }

    private void calibrateSensor() {
        // Calibration is started by setting a flag, then requesting current levels from the sensor.
        // It'll be completed in onAccelRead().
        if ((gattClient != null) && gattClient.isConnected) {
            new AlertDialog.Builder(this)
                    .setMessage(R.string.calibrate_confirm)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            calibrating = true;
                            gattClient.requestAccels();
                            dialog.dismiss();
                        }
                    })
                    .setNegativeButton(R.string.cancel, null)
                    .show();

        }
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        
        switch (requestCode) {
            case R.id.menu_settings: {
                // Settings change could affect leveling calculation, so re-run it
                loadSettings();
                onProgressChanged(null, 0, false);
                break;
            }
// For a result from ScanActivity, not currently used
//            case R.id.menu_connect: {
//                if (resultCode == RESULT_OK) {
//                    connectToSensor(data.getStringExtra(EXTRA_ADDRESS));
//                }
//                break;
//            }
        }
    }

//    private void setUnitsIcon(MenuItem item) {
//        if (!showBlocks) {
//            item.setIcon(R.drawable.ic_actionbar_block);
//        } else {
//            item.setIcon(R.drawable.ic_actionbar_in);    // TODO: support cm
//        }
//    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        // When any of the thress sliders has changed (levels or surface hardness), update the
        // results area with the new leveling solution.

        float softness;
        if (seekBar == surfaceSeekBar) {
            softness = progress;
        } else {
            softness = surfaceSeekBar.getProgress();
        }
        softness = 1 - softness / (float) surfaceSeekBar.getMax();
        if (blockType.equals(BLOCK_TYPE_WOOD)) {
            softness /= 2;    // this is kind of a guess currently
        }
        surfaceLabel.setText(getString(R.string.surface_label) + ": blocks will sink " + 
                String.format("%1.1f\"", softness));

        Heights heights;
        StringBuilder newLabel;
        float level = getLevel(seekBar, progress);
        if (seekBar == lateralSeekBar) {
            // Update the UI surrounding the slider
            newLabel = new StringBuilder(getString(R.string.lateral_label)).append(": ");
            if (level == 0) {
                newLabel.append(getString(R.string.centered));
            } else if (level < 0) {
                newLabel.append(getString(R.string.high_left, -level, unit));
            } else {
                newLabel.append(getString(R.string.high_right, level, unit));
            }
            lateralLabel.setText(newLabel);
            lateralLabel.setTextColor(getTextColor(
                    (progress < 0) || (progress > lateralSeekBar.getMax())));

            // Calculate a new leveling solution
            heights = new Heights(level, getBubble(longitudinalSeekBar), softness);

        } else if (seekBar == longitudinalSeekBar) {
            // Update the UI surrounding the slider
            newLabel = new StringBuilder(getString(R.string.longitudinal_label)).append(": ");
            if (level == 0) {
                newLabel.append(getString(R.string.centered));
            } else if (level < 0) {
                newLabel.append(getString(R.string.high_front, -level, unit));
            } else {
                newLabel.append(getString(R.string.high_rear, level, unit));
            }
            longitudinalLabel.setText(newLabel);
            longitudinalLabel.setTextColor(getTextColor(
                    (progress < 0) || (progress > longitudinalSeekBar.getMax())));

            // Calculate a new leveling solution
            heights = new Heights(getBubble(lateralSeekBar), level, softness);

        } else {
            // Surface softness has changed - this also requires a new leveling solution 
            heights = new Heights(getBubble(lateralSeekBar), getBubble(longitudinalSeekBar), softness);
        }

        // Display output
//            leftFrontHeight.setText(Utility.roundTo(heights.leftFront, 1) + unit);
//            rightFrontHeight.setText(Utility.roundTo(heights.rightFront, 1) + unit);
//            leftRearHeight.setText(Utility.roundTo(heights.leftRear, 1) + unit);
//            rightRearHeight.setText(Utility.roundTo(heights.rightRear, 1) + unit);

//        if (!showBlocks) {
//                // Display raw measurements
//                String unit = "\"";
//                leftFrontBlocks.setText(String.valueOf(heights.leftFront) + unit);
            
//        } else 
        if (heights.bestRounding != null) {
            // Display block counts
            float totalCount = 0;
            totalCount += showBlockCount(heights, Heights.LEFT_FRONT);
            totalCount += showBlockCount(heights, Heights.LEFT_REAR);
            totalCount += showBlockCount(heights, Heights.RIGHT_FRONT);
            totalCount += showBlockCount(heights, Heights.RIGHT_REAR);
            
            totalBlocks.setText(getString(R.string.total_blocks, 
                    resources.getQuantityString(pluralResource, 2),
                    totalCount));
            if (totalCount > 20) {
                if (Float.isInfinite(totalCount)) {
                    totalBlocks.setVisibility(View.INVISIBLE);
                } else {
                    totalBlocks.setTextColor(getTextColor(true));
                    totalBlocks.setVisibility(View.VISIBLE);
                }
            } else {
                totalBlocks.setTextColor(getTextColor(false));
                totalBlocks.setVisibility(View.VISIBLE);
            }
        }
    }

    private int getTextColor(boolean warning) {
        if (warning) {
            if (currentThemeId == DayTheme) {
                return Color.parseColor("#ff0000");
            } else {
                return Color.parseColor("#ff2020");
            }
        } else {
            if (currentThemeId == DayTheme) {
                return Color.BLACK;
            } else {
                return Color.WHITE;
            }
        }
    }

    // Required for OnSeekBarChangeListener interface, not needed for this implementation
    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {}
    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {}

    @Override
    public void onClick(View view) {
        if ((view instanceof TextView) && (gattClient != null) && gattClient.isConnected) {
            // Increment/decrement the appropriate level by 1 step (1/2")
            CharSequence text = ((TextView) view).getText();
            if (text.equals(getString(R.string.left))) {
                lateralSeekBar.setProgress(lateralSeekBar.getProgress() - 1);
            } else if (text.equals(getString(R.string.right))) {
                lateralSeekBar.setProgress(lateralSeekBar.getProgress() + 1);
            } else if (text.equals(getString(R.string.front))) {
                longitudinalSeekBar.setProgress(longitudinalSeekBar.getProgress() - 1);
            } else if (text.equals(getString(R.string.rear))) {
                longitudinalSeekBar.setProgress(longitudinalSeekBar.getProgress() + 1);
            }
        }
    }
    
    private float showBlockCount(Heights heights, int which) {
        int height;
        TextView[] displays;
        
        switch (which) {
            case Heights.LEFT_FRONT:
                height    = (int) heights.bestRounding.leftFront;
                displays = new TextView[] {leftFrontBlocks};
                break;
            case Heights.LEFT_REAR:
                height    = (int) heights.bestRounding.leftRear;
                displays = new TextView[] {leftRearBlocks0, leftRearBlocks1};
                break;
            case Heights.RIGHT_FRONT:
                height    = (int) heights.bestRounding.rightFront;
                displays = new TextView[] {rightFrontBlocks};
                break;
            case Heights.RIGHT_REAR:
                height    = (int) heights.bestRounding.rightRear;
                displays = new TextView[] {rightRearBlocks0, rightRearBlocks1};
                break;
            default:
                return 0;
        }

        float result = 0;
        for (TextView display : displays) {
            if (height == 0) {
                display.setText(null);
            } else {
                display.setText(Math.round(height / blockSize) + " " +
                        resources.getQuantityString(pluralResource, height));
                if (height >= Heights.BLOCK_LAYOUTS.length) {
                    display.setTextColor(Color.RED);
                    result = Float.POSITIVE_INFINITY;
                } else {
                    display.setTextColor(Color.BLACK);
                    result = Heights.countBlocks(height);
                    if (which >= Heights.LEFT_REAR) {
                        result *= rearType;
                    }
                }
            }
        }
        
        return result;
    }
    
    private float getLevel(SeekBar seekBar, int progress) {
        if (seekBar == null) {
            return 0;
        } else {
            return (progress - seekBar.getMax() / 2) / 2f;
        }
    }
    private float getBubble(SeekBar seekBar) {
        return getLevel(seekBar, seekBar.getProgress());
    }

    private void toggleTheme() {
        // Switch between Day and Night themes

        // Set up the new theme itself
        String themeName;
        if (currentThemeId == DayTheme) {
            currentThemeId = R.style.NightTheme;
            themeName = getString(R.string.theme_night);
        } else {
            currentThemeId = R.style.DayTheme;
            themeName = getString(R.string.theme_day);
        }
        setTheme(currentThemeId);

        // Cache the current values of the sliders
        int longitudinal = longitudinalSeekBar.getProgress();
        int lateral = lateralSeekBar.getProgress();
        int surface = surfaceSeekBar.getProgress();

        // Recreate the UI layout (using the new theme)
        initUI();
        if (currentThemeId == R.style.DayTheme) {
            findViewById(R.id.layout_root).setBackgroundColor(Color.WHITE);
        } else {
            findViewById(R.id.layout_root).setBackgroundColor(Color.BLACK);
        }

        // Apply the cached slider values
        longitudinalSeekBar.setProgress(longitudinal);
        lateralSeekBar.setProgress(lateral);
        surfaceSeekBar.setProgress(surface);
        onProgressChanged(null, 0, false);

        // Finish up
        settings.edit().putString("theme", themeName).apply();
        invalidateOptionsMenu();
    }
    private void toggleTorch() {
        try {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                // for M+, use CameraManager.setTorchMode()
                CameraManager cameraMgr = (CameraManager) getSystemService(CAMERA_SERVICE);
                String[] cameraIds = cameraMgr.getCameraIdList();
                for (String cameraId : cameraIds) {
                    CameraCharacteristics chars = cameraMgr.getCameraCharacteristics(cameraId);
                    //noinspection ConstantConditions
                    if (chars.get(CameraCharacteristics.FLASH_INFO_AVAILABLE) &&
                            (chars.get(CameraCharacteristics.LENS_FACING) == CameraMetadata.LENS_FACING_BACK)) {
                        isFlashOn = !isFlashOn;
                        cameraMgr.setTorchMode(cameraId, isFlashOn);
                        break;
                    }
                }
            } else {
                // pre-M flashlight support - props to http://stackoverflow.com/questions/6068803
                if (cam == null) {
                    SurfaceView preview = findViewById(R.id.camera_preview);
                    mHolder = preview.getHolder();
                    mHolder.addCallback(this);
                    cam = Camera.open();
                    cam.setPreviewDisplay(mHolder);

                    // Turn on flash
                    Parameters p = cam.getParameters();
                    p.setFlashMode(Parameters.FLASH_MODE_TORCH);
                    cam.setParameters(p);
                    cam.startPreview();
                } else {
                    // Turn off LED
                    cam.stopPreview();
                    cam.release();
                    cam = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        invalidateOptionsMenu();
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width,
            int height) {}

    public void surfaceCreated(SurfaceHolder holder) {
        mHolder = holder;
        try {
            cam.setPreviewDisplay(mHolder);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    public void surfaceDestroyed(SurfaceHolder holder) {
        if (cam != null) {
            cam.stopPreview();
        }
        mHolder = null;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION_PERMISSION: {
                if (ContextCompat.checkSelfPermission(this, ACCESS_COARSE_LOCATION)
                                == PackageManager.PERMISSION_GRANTED) {
                    // Permission has been granted - start scanning for a sensor
                    bleScanner.startScan(this);
                    showTopToast(R.string.scanning);
                }
            }
        }
    }

    @Override
    public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
        // Scan has completed successfully (a sensor has been found). Connect to it.
        bleScanner.stopScan(this);
        connectToSensor(device.getAddress());
        invalidateOptionsMenu();
    }

    @Override
    public void onLeScanTimeout() {
        // Scan completed unsuccessfully
        if ((gattClient == null) || !gattClient.isConnected) {
            promptForScan(R.string.no_devices);
            invalidateOptionsMenu();
        }
    }

    private void promptForScan(int message) {
        showTopToast(message);
/* // Eventually, we'd like to open a Scan window to help the user find their sensor.
        Snackbar.make(layoutRoot, message, Snackbar.LENGTH_LONG)
                .setAction(R.string.menu_scan, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivityForResult(new Intent(LevelActivity.this, DeviceScanActivity.class),
                                R.id.menu_connect);
                    }
                })
                .show();
*/
    }

    private void showTopToast(int message) {
        // Show a Toast message above the leveling results area in the main layout 
        if (isStarted) { // don't show Toast when activity not visible
            Toast toast = Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT);
            if (resources.getBoolean(R.bool.is_landscape)) {
                toast.setGravity(Gravity.TOP | Gravity.START,
                        resources.getDimensionPixelOffset(R.dimen.activity_horizontal_margin),
                        (int) (1.1 * resources.getDimensionPixelOffset(R.dimen.top_toast_margin)));
            } else {
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0,
                        (int) (1.1 * resources.getDimensionPixelOffset(R.dimen.top_toast_margin)));
            }
            toast.show();
        }
    }
    
    private void connectToSensor(String address) {
        // Call this with a BT address to start a connection or null to close the current connection
        if (gattClient != null) {
            // Disconnect from previous accel sensor
            gattClient.close();
            gattClient = null;
            sensorAddress = null;
            lateralSeekBar.setEnabled(true);
            longitudinalSeekBar.setEnabled(true);
        }
        if (address != null) {
            gattClient = new AccelGattClient(this, address, this);
            sensorAddress = address;
        }
        invalidateOptionsMenu();
    }

    @Override
    public void onConnected(final boolean success) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (success) {
                    Log.d(TAG, "onconnected: success");
                    showTopToast(R.string.connected);
                    lateralSeekBar.setEnabled(false);
                    longitudinalSeekBar.setEnabled(false);
                } else {
                    showTopToast(R.string.connect_error);
                    connectToSensor(null);
                }
                invalidateOptionsMenu();
            }
        });
    }

    @Override
    public void onAccelRead(final int[] values) {
        if (DOLOG.value) Log.d(TAG, "Accel X " + values[0] + "\t Accel Y " + values[1] + "\t Accel Z " + values[2]);
        
        // We have new data from the accelerometer sensor. Apply it.
        
        if (values.length != 3) {
            Log.e(TAG, "onAccelRead: bad values parm, length = " + values.length);
        } else {
            // Read the data, transforming it as needed according to the sensor's orientation in the vehicle.
            // ToDo: adjust these transforms as needed for your own sensor orientation. For production,
            // a process will need to be developed for the end user to configure this.
            final float rawX = -values[1];
            final float rawY = values[0];
            final float rawZ = values[2];
            
            if (calibrating) {
                // A calibration has been requested, so save the current accel data as a "zero point"
                // against which all future vectors will be compared.
                // Note that there's a low-pass filter on the sensor module, so we don't bother to 
                // average a number of samples together here.
                settings.edit()
                        .putFloat("calibrate_x", rawX)
                        .putFloat("calibrate_y", rawY)
                        .putFloat("calibrate_z", rawZ)
                        .apply();
                measured1g = values[2];
                calibrating = false;
            }
            
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // Apply the calibration to the raw data
                    float calibratedX = (rawX - settings.getFloat("calibrate_x", 0)) / measured1g;
                    float calibratedY = (rawY - settings.getFloat("calibrate_y", 0)) / measured1g;
                    float calibratedZ = (measured1g + rawZ - 
                            settings.getFloat("calibrate_z", measured1g)) / measured1g;

                    // Convert native accelerometer data to radians of tilt. From Equations 11 & 12 at
                    // http://www.analog.com/media/en/technical-documentation/application-notes/AN-1057.pdf
                    double theta = Math.atan2(calibratedX, 
                            Math.sqrt(calibratedY * calibratedY + calibratedZ * calibratedZ));
                    double psi = Math.atan2(calibratedY, 
                            Math.sqrt(calibratedX * calibratedX + calibratedZ * calibratedZ));

                    // Convert X-axis tilt into longitudinal inches (and apply to UI)
                    double inchesLon = Math.tan(theta) * (wheelbase * 12);
                    int progress = (int) (2 * inchesLon + (longitudinalSeekBar.getMax() / 2));
                    if (progress > longitudinalSeekBar.getMax()) {
                        longitudinalSeekBar.getThumb().setAlpha(0);
                        onProgressChanged(longitudinalSeekBar, progress, true);
                    } else {
                        longitudinalSeekBar.getThumb().setAlpha(255);
                        longitudinalSeekBar.setProgress(progress);
                    }

                    // Convert Y-axis tilt into lateral inches (and apply to UI)
                    double inchesLat = Math.tan(psi) * 90;
                    progress = (int) (2 * inchesLat + (lateralSeekBar.getMax() / 2));
                    if (progress > lateralSeekBar.getMax()) {
                        lateralSeekBar.getThumb().setAlpha(0);
                        onProgressChanged(lateralSeekBar, progress, true);
                    } else {
                        lateralSeekBar.getThumb().setAlpha(255);
                        lateralSeekBar.setProgress(progress);
                    }
                    
                    Log.d(TAG, "reported tilt: x " + Math.toDegrees(theta) + ", y " + Math.toDegrees(psi));
                }
            });
        }
    }
}
