# SmartRig RV Level

Automated leveling solutions for recreational vehicles.

Please see full writeup at 
[https://www.hackster.io/sterlingudell/smartrig-rv-level-64155d](https://www.hackster.io/sterlingudell/smartrig-rv-level-64155d)

Copyright (C) 2017 Udell Enterprises, Inc.

## License

```
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```
