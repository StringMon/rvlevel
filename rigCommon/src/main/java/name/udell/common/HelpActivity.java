package name.udell.common;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Window;
import android.webkit.WebView;

import net.smartrig.common.R;

public class HelpActivity extends Activity {
    static final String TAG = "HelpActivity";
    private static final BaseApp.LogFlag DOLOG = BaseApp.DOLOG;

    boolean versionSet = false;
//    String flurryKey = null;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set up the screen layout
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.help);
        final WebView helpView = (WebView) findViewById(R.id.help_content);
        helpView.getSettings().setBuiltInZoomControls(false);
        helpView.setBackgroundColor(Color.rgb(24, 24, 24));
        
        String url = null;
        int titleID = 0;
        Intent intent = getIntent();
        if (intent != null) {
            url            = intent.getStringExtra("url");
            titleID        =  intent.getIntExtra("title", 0);
        }

        if (titleID != 0) {
            setTitle(titleID);
        }
        
        if (url == null) {
            url = "file:///android_asset/help.html";
        }
        helpView.loadUrl(url);
    }
}
