package name.udell.common;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.preference.ListPreference;
import android.util.AttributeSet;


/*
 * A simple ListPreference descendant that shows the currently selected value as the summary. 
 */
public class SummarizedListPreference extends ListPreference {

    private CharSequence[] summaries;
    public boolean autoShowDialog = true;
    
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public SummarizedListPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        
        init(context, attrs);
    }

    public SummarizedListPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        
        init(context, attrs);
    }
    public SummarizedListPreference(Context context) {
        this(context, null);
    }

    private void init(Context context, AttributeSet attrs) {
//        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.SummarizedListPreference, 0, 0);
//        summaries = a.getTextArray(R.styleable.SummarizedListPreference_summaries);
//        a.recycle();
    }
    
    @Override
    public void setValue(String newValue) {
        super.setValue(newValue);
        
        setSummaryByValue(newValue);
    }

    public SummarizedListPreference setSummary() {
        return setSummaryByValue(getValue());
    }
    
    public SummarizedListPreference setSummaryByValue(String newValue) {
        CharSequence[] values = getEntryValues();
        int index;
        for (index = values.length - 1; index >= 0; index--) {
            if (values[index].equals(newValue)) {
                break;
            }
        }

        if (index == -1) {
            setSummary(null);
        } else {
            setSummary(getEntries()[index].toString().replace('\n', ' '));
        }

        return this;
    }
    public CharSequence[] getSummaries() {
        return summaries;
    }
    public void setSummaries(CharSequence[] summaries) {
        this.summaries = summaries;
    }

    @Override
    public Dialog getDialog() {
        return super.getDialog();
    }

    @Override
    protected void onClick() {
        if (autoShowDialog) {
            super.onClick();
        }
    }
}
