package name.udell.common;

import android.app.Application;

import net.smartrig.common.BuildConfig;
import net.smartrig.common.R;

/**
 * Application-level items, mostly used for debug logging right now.
 * 
 * Created by Sterling on 2017-10-28.
 */

public class BaseApp extends Application {
    public static boolean DEBUG_BUILD = BuildConfig.DEBUG;    // Use this instead of BuildConfig.DEBUG in your code, so when that goes awry, you have a single place to fix it.
    public static class LogFlag {
        public boolean value = DEBUG_BUILD;
    }

    public static final LogFlag DOLOG = new LogFlag();
}
