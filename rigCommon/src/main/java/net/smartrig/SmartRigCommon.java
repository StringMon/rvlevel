package net.smartrig;

import java.util.UUID;

/**
 * Constants and methods used in multiple leveling modules.
 * 
 * Created by Sterling on 2017-10-25.
 */

public class SmartRigCommon {
    public static final float ADXL345_1G = 1000f / 3.9f; // Typical gain is 3.9mg
    
    public static UUID ACCEL_SERVICE_UUID = UUID.fromString("f7c9b4f8-4562-40fb-9ad9-246ac6631816");
    public static UUID CHARACTERISTIC_ACCELS_UUID = UUID.fromString("8db491b1-047c-4ad4-95d1-e506970b2901");
    /* Mandatory Client Characteristic Config Descriptor */
    public static UUID DESCRIPTOR_CONFIG_UUID = UUID.fromString("00002902-0000-1000-8000-246ac6631816");
    public static UUID CHARACTERISTIC_CALIBRATE_UUID = UUID.fromString("9d43992d-fa79-40b3-b8ec-236deaa94a58");
}
