/*
 * Copyright 2017, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.smartrig.level;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.util.Log;

import static net.smartrig.SmartRigCommon.ACCEL_SERVICE_UUID;
import static net.smartrig.SmartRigCommon.ADXL345_1G;
import static net.smartrig.SmartRigCommon.CHARACTERISTIC_ACCELS_UUID;
import static net.smartrig.SmartRigCommon.CHARACTERISTIC_CALIBRATE_UUID;
import static net.smartrig.SmartRigCommon.DESCRIPTOR_CONFIG_UUID;

/**
 * A custom GATT profile for delivering accelerometer data.
 *
 * Created by Sterling on 2017-10-25.
 */
@SuppressWarnings("WeakerAccess")
public class AccelProfile {
    private static final String TAG = AccelProfile.class.getSimpleName();

    private static float[] lastAccelValues = new float[] {0, 0, ADXL345_1G};
    private static long lastUpdate = 0;

    /**
     * Return a configured instance for the accelerometer service.
     */
    public static BluetoothGattService createService() {
        BluetoothGattService service = new BluetoothGattService(ACCEL_SERVICE_UUID,
                BluetoothGattService.SERVICE_TYPE_PRIMARY);

        // Current Acceleration vector characteristic
        BluetoothGattCharacteristic currentAccels = new BluetoothGattCharacteristic(CHARACTERISTIC_ACCELS_UUID,
                //Read-only characteristic, supports notifications
                BluetoothGattCharacteristic.PROPERTY_READ | BluetoothGattCharacteristic.PROPERTY_NOTIFY,
                BluetoothGattCharacteristic.PERMISSION_READ);
        BluetoothGattDescriptor config = new BluetoothGattDescriptor(DESCRIPTOR_CONFIG_UUID,
                //Read/write descriptor
                BluetoothGattDescriptor.PERMISSION_READ | BluetoothGattDescriptor.PERMISSION_WRITE);
        currentAccels.addDescriptor(config);
        service.addCharacteristic(currentAccels);

        // Calibration characteristic
        BluetoothGattCharacteristic calibrator = new BluetoothGattCharacteristic(CHARACTERISTIC_CALIBRATE_UUID,
                BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE,
                BluetoothGattCharacteristic.PERMISSION_WRITE);
        service.addCharacteristic(calibrator);
        
        return service;
    }

    /**
     * Construct the field values for the last recorded accelerometer data.
     */
    public static byte[] getAccels() {
        // The accel values SHOULD be in m/s^2. However, my current ADXL345 driver feeds in the 
        // raw values from the chip (as ints). So for now, just extract them to bytes for BLE 
        // transmission. 
        byte[] accelVector = new byte[6];
        for (int i = 0; i < 3; i++) {
            int intValue = (int) lastAccelValues[i];
            accelVector[2 * i] = (byte) (intValue & 0xff);
            accelVector[2 * i + 1] = (byte) ((intValue & 0xff00) >> 8);
        }
        return accelVector;
    }

    /**
     * Save accelerometer data for later transmission.
     * 
     * @param accelValues the 3-dimensional accelration vector to save: [x, y, z]
     * @return whether this saved data should be transmitted to any connected BLE clients
     */
    public static boolean setAccels(float... accelValues) {
        boolean result = false;
        final long now = System.currentTimeMillis();
        if (now - lastUpdate > 5_000) {
            result = true;
        } else {
            final float threshold = 1f;
            for (int i = 0; i < accelValues.length; i++) {
                if (Math.abs(accelValues[i] - lastAccelValues[i]) > threshold) {
                    result = true;
                    break;
                }
            }
        }
        if (result) {
            Log.d(TAG, "Filtered X " + accelValues[0] + "\t Filtered Y " + accelValues[1] + "\t Filtered Z " + accelValues[2]);
            lastAccelValues = accelValues.clone();
            lastUpdate = now;
        }
        return result;
    }
}
