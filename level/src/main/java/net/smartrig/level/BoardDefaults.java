package net.smartrig.level;

import android.os.Build;

import com.google.android.things.pio.PeripheralManagerService;

import java.util.List;

/**
 * Interface constants for various Android Things boards.
 * 
 * Based on https://github.com/cagdasc/AndroidThings-ADXL345
 */

@SuppressWarnings("WeakerAccess")
public class BoardDefaults {
    private static final String DEVICE_EDISON_ARDUINO = "edison_arduino";
    private static final String DEVICE_EDISON = "edison";
    private static final String DEVICE_JOULE = "joule";
    private static final String DEVICE_RPI3 = "rpi3";
    private static final String DEVICE_IMX6UL_PICO = "imx6ul_pico";
    private static final String DEVICE_IMX6UL_VVDN = "imx6ul_iopb";
    private static final String DEVICE_IMX7D_PICO = "imx7d_pico";
    private static String sBoardVariant = "";

    /**
     * Return the preferred I2C port for each board.
     */
    public static String getI2CPort() {
        switch (getBoardVariant()) {
            case DEVICE_EDISON_ARDUINO:
                return "I2C6";
            case DEVICE_EDISON:
                return "I2C1";
            case DEVICE_JOULE:
                return "I2C0";  // from https://github.com/intel-iot-devkit/android-things-samples/tree/master/tmp006
            case DEVICE_RPI3:
                return "I2C1";
            case DEVICE_IMX6UL_PICO:
                return "I2C2";  // This one's a guess
            case DEVICE_IMX6UL_VVDN:
                return "I2C2";  // ditto
            default:
                // Note I don't have an I2C bus address for the IMX7D at this time
                throw new IllegalStateException("Unknown Build.DEVICE " + Build.DEVICE);
        }
    }

    public static String getReadyGPIO() {
        switch (getBoardVariant()) {
            case DEVICE_EDISON_ARDUINO:
                return "IO2";
            case DEVICE_EDISON:
                return "GP45";
            case DEVICE_JOULE:
                return "J6_25";
            case DEVICE_RPI3:
                return "BCM6";
            case DEVICE_IMX6UL_PICO:
                return "GPIO4_IO22";
            case DEVICE_IMX6UL_VVDN:
                return "GPIO3_IO06";
            case DEVICE_IMX7D_PICO:
                return "GPIO_34";
            default:
                throw new IllegalStateException("Unknown Build.DEVICE " + Build.DEVICE);
        }
    }

    public static String getConnectedGPIO() {
        switch (getBoardVariant()) {
            case DEVICE_EDISON_ARDUINO:
                return "IO3";
            // Note that the GPIO port numbers below here are the same as for the Ready LED,
            // because I currently only have an Edison Arduino to develop on.
            // ToDo: find another GPIO port from PeripheralManagerService 
            case DEVICE_EDISON:
                return "GP45";
            case DEVICE_JOULE:
                return "J6_25";
            case DEVICE_RPI3:
                return "BCM6";
            case DEVICE_IMX6UL_PICO:
                return "GPIO4_IO22";
            case DEVICE_IMX6UL_VVDN:
                return "GPIO3_IO06";
            case DEVICE_IMX7D_PICO:
                return "GPIO_34";
            default:
                throw new IllegalStateException("Unknown Build.DEVICE " + Build.DEVICE);
        }
    }

    private static String getBoardVariant() {
        if (!sBoardVariant.isEmpty()) {
            return sBoardVariant;
        }
        sBoardVariant = Build.DEVICE;
        // For the edison check the pin prefix
        // to always return Edison Breakout pin name when applicable.
        if (sBoardVariant.equals(DEVICE_EDISON)) {
            PeripheralManagerService pioService = new PeripheralManagerService();
            List<String> gpioList = pioService.getGpioList();
            if (gpioList.size() != 0) {
                String pin = gpioList.get(0);
                if (pin.startsWith("IO")) {
                    sBoardVariant = DEVICE_EDISON_ARDUINO;
                }
            }
        }
        return sBoardVariant;
    }
}
