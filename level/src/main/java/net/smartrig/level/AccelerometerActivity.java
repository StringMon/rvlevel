/*
 * Copyright 2016 Cagdas Caglak
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.smartrig.level;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;

import com.cacaosd.adxl345.ADXL345SensorDriver;

import java.io.IOException;

import name.udell.common.BaseApp;

import static net.smartrig.SmartRigCommon.ADXL345_1G;

/**
 * Main activity for the Android Things accelerometer interface.
 *
 * Created by Sterling on 2017-10-24.
 * Based on https://github.com/cagdasc/AndroidThings-ADXL345/tree/master/sample/src/main/java/com/cacaosd/sample
 */
public class AccelerometerActivity extends Activity implements SensorEventListener {
    private static final BaseApp.LogFlag DOLOG = BaseApp.DOLOG;
    private static final String TAG = AccelerometerActivity.class.getSimpleName();

    private ADXL345SensorDriver mSensorDriver;
    private SensorManager mSensorManager;
    private AccelGattServer gattServer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mSensorManager.registerDynamicSensorCallback(new SensorManager.DynamicSensorCallback() {
            @Override
            public void onDynamicSensorConnected(Sensor sensor) {
                if (sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                    mSensorManager.registerListener(AccelerometerActivity.this,
                            sensor, SensorManager.SENSOR_DELAY_NORMAL);
                }
            }
        });

        try {
            mSensorDriver = new ADXL345SensorDriver(BoardDefaults.getI2CPort(), 2);
            mSensorDriver.registerAccelerometerSensor();
        } catch (IOException e) {
            Log.e(TAG, "Error configuring sensor", e);
        }

        gattServer = new AccelGattServer(this);
    }

    @Override
    protected void onDestroy() {
        if (DOLOG.value) Log.i(TAG, "Closing sensor");
        if (mSensorDriver != null) {
            mSensorManager.unregisterListener(this);
            try {
                mSensorDriver.close();
            } catch (IOException e) {
                Log.e(TAG, "Error closing sensor", e);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                mSensorDriver = null;
            }
        }
        
        gattServer.close();
        
        super.onDestroy();
    }

    float[] gravity = new float[] {0, 0, ADXL345_1G};
    
    @Override
    public void onSensorChanged(SensorEvent event) {
        if (DOLOG.value) Log.v(TAG, "Accel X " + event.values[0] + "\t Accel Y " + event.values[1] + "\t Accel Z " + event.values[2]);

        // Apply a low-pass filter to isolate the acceleration of gravity
        final float alpha = 0.9f;
        gravity[0] = alpha * gravity[0] + (1 - alpha) * event.values[0];
        gravity[1] = alpha * gravity[1] + (1 - alpha) * event.values[1];
        gravity[2] = alpha * gravity[2] + (1 - alpha) * event.values[2];
        
        if (AccelProfile.setAccels(gravity)) {
            gattServer.notifyRegisteredDevices();
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        if (DOLOG.value) Log.i(TAG, "sensor accuracy changed: " + accuracy);
    }
}
